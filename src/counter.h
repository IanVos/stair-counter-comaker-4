/**
 * @file counter.h
 * @author your name (you@domain.com)
 * @brief 
 * @version 0.1
 * @date 2022-06-08
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#include <iostream>
#include <Arduino.h>
#include "MedianFilter.h"
#include <cmath>

#define FILT_SIZE 5
#define THRESHOLD 250
#define NO_DEBUG
// #define DISTANCE

enum direction_t {
    NONE,
    DOWN,
    UP
};
enum sensor_state_t {
  no_detection,
  detection
};

struct sensor_value{
    uint16_t sensor1;
    uint16_t sensor2;
};
struct sensor_detection{
    sensor_state_t sensor1;
    sensor_state_t sensor2;
};
class counter {
private:
    uint8_t _sensor_1;
    uint8_t _sensor_2;
    sensor_value _prev_value;
    sensor_value _calibration;

    sensor_detection _prev_state;

    uint32_t _interation_num;
    uint8_t _iteration_timeout = 50;
    uint8_t _wait = 0;

    MedianFilter dist_sensor_1 = MedianFilter(FILT_SIZE, 1500);
    MedianFilter dist_sensor_2 = MedianFilter(FILT_SIZE, 1500);

    sensor_value get_sensors_raw(); 
    sensor_value get_sensors_dist(sensor_value raw_values);
    sensor_value filter_values(sensor_value values);
    sensor_state_t check_sensor(uint16_t value, uint16_t last_value);
    sensor_detection detect_objects(sensor_value values, sensor_value last_values);
    direction_t check_sens_dir(sensor_state_t current, sensor_state_t other);
    direction_t check_direction(sensor_detection state);
    void update_states(sensor_detection current_states);

public:
    counter();
    counter(uint8_t sens1, uint8_t sens2);
    ~counter();
    
    void begin(uint8_t sens1, uint8_t sens2);
    direction_t getDirection();
    sensor_value calibrate(uint8_t samples, uint32_t cal_delay);
};