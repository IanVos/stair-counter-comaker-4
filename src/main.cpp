/**
 * @file main.cpp
 * @author Ian Vos <ian.vos@hva.nl>
 * @brief Count people walking up and down stairs using an IR distance sensor. 
 * 
 * Project Walk up and Down tries to motivate people to take the stairs instead of the elevator.
 * The amout of people taking the stairs or elevator is counted by the strair counter box
 * To motivate people a web interface is used to display the number of people taking the stairs
 * This is done in a playfull/challaging manner with goals that increase
 * 
 * This software uses the Sharp infrared distance sensor provided by the minor Zorgtechnologie.
 * The sensor box is placed facing the stairs and counts how many people walk past it and the direction
 * The software will use aproximatly ten seconds before starting calibration
 * 
 * This file contains basic startup code and the wifi portion of the software
 *  - For settings see: local.h
 *  - For counter specific software see the counter class (counter.h)
 * 
 * @version 1.0
 * 
 * @date 2022-05-05
 * @copyright Copyright (c) 2022
 * 
 **/

#include <Arduino.h>

#include <WiFi.h>
#include <HTTPClient.h>

#include "local.h"
#include "counter.h"

/**
 * @brief define pin numbers used and basic needed values
 */
#define SENSOR_1 34   // IO34, 10, ADC01_6
#define SENSOR_2 35  // IO35, 11, ADC01_7

#define TIME_DELAY 100  // Delay between each measurement

#define NO_DEBUG // Set no debug flag (if not set debug code is active)

/**
 * @brief Get WIFI settings from local file
 */
const char* ssid = WIFI_SSID;
const char* password = WIFI_PASS;
String serverName = SERVER_URL;
String device_id = DEVICE_ID; // Create a string with the defice ID
String data_preurl = "sendData.php?d=" + device_id; // Identify as current sensor

counter stair_counter;  // Init empty counter instance

/**
 * @brief Set the up wifi object
 * 
 * @return int, 0 if success
 */
int setup_wifi() {
  WiFi.begin(ssid, password);
  int tries = 0;
  while (WiFi.status() != WL_CONNECTED) {
    delay(1000);
    Serial.println("Connecting to WiFi..");
    if(tries > 10){
      return 1;
    }
    tries++;
  }
  Serial.println("Connected to the WiFi network");
  return 0;
}

/**
 * @brief Send data over wifi
 * 
 * @param payload (String) the data to send
 */
void wifi_send(String payload) {
  if(WiFi.status()== WL_CONNECTED){
      HTTPClient http;

      String serverPath = serverName + payload;
      
      // Your Domain name with URL path or IP address with path
      http.begin(serverPath.c_str());
      
      // Send HTTP GET request
      int httpResponseCode = http.GET();
      
      if (httpResponseCode>0) {
        String payload = http.getString();
      } else {
        Serial.print("Error code: ");
        Serial.println(httpResponseCode);
      }
      // Free resources
      http.end();
    } else {
      Serial.println("WIFI Not connected");
      delay(100);
    }
}

/**
 * @brief Setup pin modes, serial and wifi
 * 
 */
void setup() {

  Serial.begin(115200);

  setup_wifi();
  delay(200);
  
  /* Register device with ID from settings */
  wifi_send("deviceRegister.php?id=" + device_id);

  stair_counter.begin(SENSOR_1, SENSOR_2);
  delay(10000);
}

/**
 * @brief Check if there is a new person counted in direction
 * 
 */
void loop() {

  direction_t direction = stair_counter.getDirection();
  switch (direction) {
  case NONE:
    break;
  case UP:
    wifi_send(data_preurl + "&direction=up");
    Serial.println("Going Up");
    break;
  case DOWN:
    wifi_send(data_preurl + "&direction=down");
    Serial.println("Going Down");
    break;
  }

  #ifndef NO_DEBUG
    Serial.printf("Dir: %d\n", direction);
  #endif

  delay(TIME_DELAY);
}