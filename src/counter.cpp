#include "counter.h"

/**
 * @brief Construct a new counter::counter object
 * 
 * Create an empty counter instance.
 * Must use counter::begin() before getting data!
 * 
 * Used for global creation before a proper setup is done
 * 
 */
counter::counter(){

}

/**
 * @brief Construct a new counter::counter object
 * 
 * @param sens1 uint8_t the gpio number of the first sensor
 * @param sens2 uint8_t the gpio number of the second sensor
 */
counter::counter(uint8_t sens1, uint8_t sens2) {
    /**
     * @brief begin counter object
     */
    begin(sens1, sens2);
}

counter::~counter() {
}

/**
 * @brief Begin the counter object
 * 
 * Begin initialises all needed variables and sets pins to input
 * It presets the previous value
 * It begins calibration
 * 
 * @param sens1 uint8_t the gpio number of the first sensor
 * @param sens2 uint8_t the gpio number of the second sensor
 */
void counter::begin(uint8_t sens1, uint8_t sens2){
    /* Set variables to private */
    _sensor_1 = sens1;
    _sensor_2 = sens2;

    /* Set the pins to input */
    pinMode(_sensor_1, INPUT);
    pinMode(_sensor_2, INPUT);

    delay(200);
    // Update the previous value to not count 1 at start
    _prev_value = get_sensors_raw();
    #ifdef DISTANCE
    _prev_value = get_sensors_dist(_prev_value);
    #endif

    _calibration = calibrate(50, 500);
}

/**
 * @brief Get the direction value of the sensors
 * 
 * Check if there is a person and return its direction
 * Wait if sensors are still high
 * 
 * @return direction_t the direction a person is walking
 *  NONE if no person detect
 *  UP if Sensor 1 triggerd first
 *  DOWN if sensor 2 triggerd first
 */
direction_t counter::getDirection(){

    /* Poll the sensors and check if it has detected something */
    sensor_value current_measure = get_sensors_raw();
    #ifdef DISTANCE
    current_measure = get_sensors_dist(current_measure);
    #endif
    current_measure = filter_values(current_measure);
    sensor_detection detection = detect_objects(current_measure, _prev_value);
    #ifndef NO_DEBUG
        Serial.printf("1: %d, 2: %d\n", current_measure.sensor1, current_measure.sensor2);
        Serial.printf("1: %d, 2: %d\n\n", detection.sensor1, detection.sensor2);
    #endif
    /* Check if we need to wait after a hit */
    if(_wait != 0){
        Serial.println("Waiting...");
        if(detection.sensor1 == no_detection && detection.sensor2 == no_detection){
            Serial.println("Reset wait...");
            _wait = 0;
        }
        update_states(detection);
        _prev_value = current_measure;
        return NONE;
    }

    /* Check if someone went up or down */
    direction_t direction = check_direction(detection);

    /* Update the previous vars for use in next iteration */
    update_states(detection);
    _prev_value = current_measure;

    return direction;
}

/**
 * @brief Get the raw sensor values
 * 
 * Read the ADC to get the raw data from both sensors
 * 
 * @return sensor_value the raw data of both sensors
 */
sensor_value counter::get_sensors_raw(){
    // Get analog value from sensor
    sensor_value raw_values;
    raw_values.sensor1 = analogRead(_sensor_1);
    raw_values.sensor2 = analogRead(_sensor_2);
    return raw_values;
}

/**
 * @brief Convert the raw_values to distance values
 * 
 * Use the power fit functions from sharp distance sensor
 * 
 * @param raw_values 
 * @return sensor_value 
 */
sensor_value counter::get_sensors_dist(sensor_value raw_values){
    // Calculate actual distance
    sensor_value distance_values;
    /* Contrain the values */
    raw_values.sensor1 = constrain(raw_values.sensor1, 1, 1023);
    raw_values.sensor2 = constrain(raw_values.sensor2, 1, 1023);
    distance_values.sensor1 = 116483.399834958 * pow(raw_values.sensor1, -1.132373887917551);
    distance_values.sensor2 = 116483.399834958 * pow(raw_values.sensor2, -1.132373887917551);
    return distance_values;
}

/**
 * @brief use a median filter to smooth out values
 * 
 * Use a median filter of size 5 to smooth out sensor readings
 * 
 * @param values 
 * @return sensor_value 
 */
sensor_value counter::filter_values(sensor_value values){
    // Filter sensor values
    sensor_value filter_values;
    filter_values.sensor1 = dist_sensor_1.in(values.sensor1);
    filter_values.sensor2 = dist_sensor_2.in(values.sensor2);
    return filter_values;
}

/**
 * @brief Check the sensor for detection
 * 
 * Check if the sensor detects someone
 * 
 * @param value 
 * @param last_value 
 * @return sensor_state_t 
 */
sensor_state_t counter::check_sensor(uint16_t value, uint16_t last_value){
    if( abs(value - last_value) > THRESHOLD ){
        return detection;
    }
    return no_detection;
}

/**
 * @brief check all sensors to get a detection object
 * 
 * @param values the values to check
 * @param last_values the values to compaire to
 * @return sensor_detection 
 */
sensor_detection counter::detect_objects(sensor_value values, sensor_value last_values){
    // Detect person
    sensor_detection detect;
    detect.sensor1 = check_sensor(values.sensor1, _calibration.sensor1);
    detect.sensor2 = check_sensor(values.sensor2, _calibration.sensor2);
    return detect;
}

/**
 * @brief Check what direction has been triggerd
 * 
 * @param state 
 * @return direction_t 
 */
direction_t counter::check_direction(sensor_detection state){
    if( _prev_state.sensor1 == detection ) {
        _interation_num++;
        // Check if sensor 2 is high then Up
        if( state.sensor2 == detection ){
            _wait = 1; // Set wait
            return UP;
        } 
    }
    if( _prev_state.sensor2 == detection ) {
        _interation_num++;
        // Check if sensor 2 is high then Up
        if( state.sensor1 == detection ){
            _wait = 1; // Set wait
            return DOWN;
        } 
    }

    return NONE;
}

/**
 * @brief Update the previous state
 * 
 * @param current_states 
 */
void counter::update_states(sensor_detection current_states){

    if(current_states.sensor1 == detection){
        _prev_state.sensor1 = current_states.sensor1;
    }
    if( current_states.sensor2 == detection ){
        _prev_state.sensor2 = current_states.sensor2;
    }
    if(_interation_num > _iteration_timeout){
        // Reset states and iteration count
        Serial.println("Reset i");
        _interation_num = 0;
        _prev_state = current_states;
    }
}

/**
 * @brief Create a basic state to compare 
 * 
 * @param samples how many samples will be taken
 * @param cal_delay how long between samples
 * @return sensor_value 
 */
sensor_value counter::calibrate(uint8_t samples, uint32_t cal_delay){
    sensor_value total, raw, calibration = {};
    total.sensor1 = 0;
    total.sensor2 = 0;
    for (uint8_t i = 0; i < samples; i++) {
        raw = get_sensors_raw();
        total.sensor1 += raw.sensor1;
        total.sensor2 += raw.sensor2;
        delay(cal_delay);
    }
    calibration.sensor1 = total.sensor1 / samples;
    calibration.sensor2 = total.sensor2 / samples;
    return calibration;
}